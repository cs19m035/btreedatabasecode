#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#ifndef BTREE_H
#define BTREE_H
#include <limits.h>
#define BTREE_DEGREE 2
#define BTREE_NUM_ENTRIES 2*BTREE_DEGREE
#define BTREE_NUM_CHILDREN BTREE_NUM_ENTRIES+1

typedef struct BTreeEntry {
  int key;
  void *value;
} BTreeEntry;

struct BTreeNode {
  BTreeEntry *entries[BTREE_NUM_ENTRIES];
  struct BTreeNode *children[BTREE_NUM_CHILDREN];
  int next_index;
};

typedef struct BTreeNode BTreeNode;

typedef struct BTree {
  BTreeNode *root;
} BTree;

BTree *btree_create();
int btree_free(BTree *btree);
void *btree_search(BTree *btree, int key);
void btree_insert(BTree *btree, int key, void *value);
#endif
#ifndef UTIL_H
#define UTIL_H

#define FALSE 0
#define TRUE 1
#define BOOL short

#endif
#include "hash.h"
#include "vec.h"

#ifndef DATABASE_H
#define DATABASE_H

/* type definitions */
typedef struct Column {
  char *name;
} Column;

typedef struct Row {
  hash_t *cells; /* col name -> string value */
} Row;

typedef vec_t(Row*) row_vec_t;

typedef struct Table {
  char *name;
  hash_t *columns;
  row_vec_t *rows;
  char *keycol;
  int numofrows,numcols;
} Table;

/* function headers */
void init_db();
void free_db();
int find_table_index(char *table_name);
Table *find_table(char *table_name);
Column *find_column(Table *table, char *column_name);
int create_table(char *name,int numcols);
int drop_table(Table *table);
int add_column(Table *table, char *column_name);
int drop_column(Table *table, Column *column);
int drop_row(Table *table, Row *row);
int insert_into(Table *table, char *column_names[], char *values[], int num_values);
/* returns pointer to a cell hash which has to be appropriately freed! */
hash_t *select_from(Table *table, char *column_names[], int num_columns, int row_index);
int list_columns(Table *table);
int list_tables();
int where_equals(char *table_name, char *column_name[], char *value[],int colcnt,int *arr,int cnt);

#endif
/* private */
hash_t *tables;

void init_db() {
  tables = hash_new();
}

void free_db() {
  hash_each_val(tables, {
    drop_table((Table *) val);
  })
  hash_free(tables);
}

Table *find_table(char *table_name) {
  return hash_get(tables, table_name);
}

int create_table(char *name,int cols) {
  Table *table = malloc(sizeof(Table));
  table->name = name;
  table->columns = hash_new();
  table->rows = malloc(sizeof(*table->rows));
  table->numofrows=0;
  table->numcols=cols;
  //table->keycol=(char*)malloc(30*sizeof(char));
  vec_init(table->rows);
  hash_set(tables, name, table);

  return 0;
}

int drop_table(Table *table) {
  if (!table) {
    return -1;
  }

  /* free columns */
  hash_each_val(table->columns, {
    drop_column(table, (Column *) val);
  })
  hash_free(table->columns);

  /* free rows */
  /* I have to store the rows in a temp array,
   * because otherwise I'd be modifying the structure I'm looping over */
  int row_num = table->rows->length;
  Row *rows[row_num];

  int i;
  Row *row;
  vec_foreach(table->rows, row, i) {
    rows[i] = row;
  }
  for (i = 0; i < row_num; i++) {
    drop_row(table, rows[i]);
  }

  vec_deinit(table->rows);
  free(table->rows);

  /* free table */
  free(table);
  hash_del(tables, table->name);

  return 0;
}

int add_column(Table *table, char *column_name) {
  if (table == NULL) {
    return -1;
  }

  Column *column = malloc(sizeof(Column)); 
  column->name = column_name;
  hash_set(table->columns, column_name, column);

  return 0;
}

int drop_column(Table *table, Column *column) {
  hash_del(table->columns, column->name);

  /* drop for each row */
  Row *row;
  int i;
  vec_foreach(table->rows, row, i) {
    hash_del(row->cells, column->name);
  }

  free(column);

  return 0;
}

int drop_row(Table *table, Row *row) {
  hash_free(row->cells);
  vec_remove(table->rows, row);
  free(row);
  //printf("A row is deleted\n");
}

Column *find_column(Table *table, char *column_name) {
  return hash_get(table->columns, column_name);
}

int insert_into(Table *table, char *column_names[], char *values[], int num_values) {
  if (!table) {
    return -1;
  }

  Row *row = malloc(sizeof(Row));
  row->cells = hash_new();
  for (int i = 0; i < num_values; i++) {
    /* this is not schemaless! only allow access to defined columns */
    Column *column = find_column(table, column_names[i]);
    if (!column) {
      return -1;
    }

    hash_set(row->cells, column_names[i], values[i]);  
  }
  vec_push(table->rows, row);

  return 0;
}

hash_t *select_from(Table *table, char *column_names[], int num_columns, int row_index) {
  if (row_index == -1) {
    return NULL;
  }

  if (!table) {
    return NULL;
  }

  if (row_index >= table->rows->length) {
    return NULL;
  }
  Row *row = table->rows->data[row_index];
  if (!row) {
    return NULL;
  }

  hash_t *result = hash_new();
  for (int i = 0; i < num_columns; i++) {
    /* this is not schemaless! only allow access to defined columns */
    Column *column = find_column(table, column_names[i]);
    if (!column) {
      hash_free(result);
      return NULL;
    }

    hash_set(result, column_names[i], hash_get(row->cells, column_names[i]));
  }

  return result;
}

int list_columns(Table *table) {
  hash_each_key(table->columns, {
    printf("%s\n", key);
  })

  return 0;
}

int list_tables() {
  hash_each(tables, {
    printf("Table %s\n", key);
    puts("COLUMNS:");
    list_columns((Table *) val);
    puts("");
  })

  return 0;
}

int where_equals(char *table_name, char *column_name[], char *value[],int colcnt,int *arr,int cnt) {
  Table *table = find_table(table_name);
  if (!table) {
    return -1;
  }

  int row_index = -1;
  Row *row;
  int i;
  int flag=0;
 
  vec_foreach(table->rows, row, i) {
     for(int j=0;j<colcnt;j++)
 {
   Column *column = find_column(table, column_name[j]);
  if (!column) {
    return -1;
  }
    if (strcmp(hash_get(row->cells, column_name[j]), value[j]) == 0) {
      
    flag=1;
    }
   else
   {flag=0;
  break;}
  }
     if(flag){
     arr[cnt]=i;
         printf("%d ",arr[cnt]);
     cnt++;}
 }
  return cnt;
}
struct BtreeKey
{
	int roll_no;
	int record;
};
struct BtreeNode
{
   struct BtreeKey *keys;
   struct BtreeNode **ptrs;
   int leaf;
   int n;
   int mindeg;
};
int mind;
int Predecessor(int,struct BtreeNode*);
int Successor(int,struct BtreeNode*);
void fill(int,struct BtreeNode*);
void Prevborrow(int,struct BtreeNode*);
void Nextborrow(int,struct BtreeNode*);
void Merge(int,struct BtreeNode*);
void LeafDeletion(int,struct BtreeNode*);
void NonLeafDeletion(int,struct BtreeNode*);
int getkey(int,struct BtreeNode*);
void deletion(int,struct BtreeNode*);
int Predecessor(int index,struct BtreeNode *root)
  {
     struct BtreeNode *temp=root->ptrs[index];
     while(!temp->leaf)
       temp=temp->ptrs[temp->n];
     
     return temp->keys[temp->n-1].roll_no;
  }
int Successor(int index,struct BtreeNode *root)
  {
    struct BtreeNode *temp=root->ptrs[index+1];
    while(!temp->leaf)
       temp=temp->ptrs[0];
     
     return temp->keys[0].roll_no;  
  }
  void fill(int index,struct BtreeNode *root)
  {
     if(index!=0 && root->ptrs[index-1]->n>=root->mindeg)
        Prevborrow(index,root);
     else if(index!=root->n && root->ptrs[index+1]->n>=root->mindeg)
        Nextborrow(index,root);

     else
     {
        if(index!=root->n)
          Merge(index,root);
        else
          Merge(index-1,root);
     }
  }
  void Prevborrow(int index,struct BtreeNode *root)
  {
     struct BtreeNode *fstchild=root->ptrs[index];
     struct BtreeNode *secchild=root->ptrs[index-1];

     int i,j;
     for(i=fstchild->n-1;i>=0;i--)
         fstchild->keys[i+1]=fstchild->keys[i];

     if(!fstchild->leaf)
     {
         for(i=fstchild->n;i>=0;i--)
             fstchild->ptrs[i+1]=fstchild->ptrs[i];
     }
     fstchild->keys[0]=root->keys[index-1];
     if(!fstchild->leaf)
        fstchild->ptrs[0]=fstchild->ptrs[secchild->n];

      root->keys[index-1]=secchild->keys[secchild->n-1];

      fstchild->n+=1;
      secchild->n-=1;
      
  }
  void Nextborrow(int index,struct BtreeNode *root)
  {
 struct  BtreeNode *fstchild=root->ptrs[index];
   struct BtreeNode *secchild=root->ptrs[index+1];
   fstchild->keys[fstchild->n]=root->keys[index];
   
   if(!(fstchild->leaf))
     fstchild->ptrs[(fstchild->n)+1]=secchild->ptrs[0];
   
   root->keys[index]=secchild->keys[0];
   int i,j;
   
   for(i=1;i<secchild->n;i++)
     secchild->keys[i-1]=secchild->keys[i];
   
    if(!secchild->leaf)
    {
        for(i=1;i<=secchild->n;i++)
            secchild->ptrs[i-1]=secchild->ptrs[i];
    }

    secchild->n-=1;
    fstchild->n+=1;
    
  }
  void Merge(int index,struct BtreeNode *root)
  {
     struct BtreeNode *fstchild=root->ptrs[index];
      struct BtreeNode *secchild=root->ptrs[index+1];
    
      fstchild->keys[root->mindeg-1]=root->keys[index];

      int i,j;
 
      for(i=0;i<secchild->n;i++)
        fstchild->keys[i+root->mindeg]=secchild->keys[i];

      if(!fstchild->leaf)
      {
          for(i=0;i<=secchild->n;i++)
               fstchild->ptrs[i+root->mindeg]=secchild->ptrs[i];
      }

      for(i=index+1;i<root->n;i++)
         root->keys[i-1]=root->keys[i];

      for(i=index+2;i<=root->n;i++)
         root->ptrs[i-1]=root->ptrs[i];

      fstchild->n+=secchild->n;
      fstchild->n+=1;
    
      root->n--;
   
      free(secchild);
  }
void LeafDeletion(int index,struct BtreeNode *root)
  {
      for(int i=index+1;i<root->n;i++)
        root->keys[i-1]=root->keys[i];
      root->n--;
  }
  void NonLeafDeletion(int index,struct BtreeNode *root)
  {
      int d=root->keys[index].roll_no;
      int prev,next;
      if(root->ptrs[index]->n>=root->mindeg)
      {
         prev=Predecessor(index,root);
         root->keys[index].roll_no=prev;
         deletion(prev,root->ptrs[index]);
      }

      else if(root->ptrs[index+1]->n>=root->mindeg)
      {
         next=Successor(index,root);
         root->keys[index].roll_no=next;
         deletion(next,root->ptrs[index+1]);
      }
      
      else
      {
          Merge(index,root);
          deletion(d,root->ptrs[index]);
      } 
  }
int getkey(int d,struct BtreeNode *root)
  {
    int i=0;
    
    while(i<root->n && root->keys[i].roll_no<d)
      i++;
    return i;
  }
void deletion(int d,struct BtreeNode *root)
  {
   int i=getkey(d,root);
   int f;
   if(i<root->n && root->keys[i].roll_no==d)
   {
       if(root->leaf)
          LeafDeletion(i,root);
       else
          NonLeafDeletion(i,root);
   }
   else
   {
     if(root->leaf)
     {
        printf("No such key is present");
        return;
     }
    f=(i==root->n);
    if(root->ptrs[i]->n<root->mindeg)
     fill(i,root);
    
    if(f&&i>root->n)
      deletion(d,root->ptrs[i-1]);
    else
      deletion(d,root->ptrs[i]);
    }
  }
void splitnode(struct BtreeNode*,int,struct BtreeNode*);
void ins(int k,struct BtreeNode *root,int rec)
  {
    int j=root->n-1;
    if(root->leaf==1)
    {
     while(j>=0&&k<root->keys[j].roll_no)
     {
        root->keys[j+1]=root->keys[j];
        j--;
     }
    root->keys[j+1].roll_no=k;
    root->keys[j+1].record=rec;
    root->n++;
    }
   else
    {
      while(j>=0&&k<root->keys[j].roll_no)
      j--;
      if(root->ptrs[j+1]->n==2*root->mindeg-1)
      {
           splitnode(root->ptrs[j+1],j+1,root);
           if(root->keys[j+1].roll_no<k)
            j++;
      }
      ins(k,root->ptrs[j+1],rec);
    }
  }
  void splitnode(struct BtreeNode *y,int i,struct BtreeNode *root)
  {
     struct BtreeNode *z=(struct BtreeNode*)malloc(sizeof(struct BtreeNode));
     z->leaf=y->leaf;
     z->mindeg=y->mindeg;
     z->keys=(struct BtreeKey*)malloc(sizeof(struct BtreeKey)*(2*root->mindeg-1));
     z->n=0;
     z->ptrs=(struct BtreeNode**)malloc(sizeof(struct BtreeNode*)*(2*root->mindeg));
     z->n=root->mindeg-1;
	if(y->leaf==0)
     	{
         for(int k=0;k<root->mindeg;k++)
            z->ptrs[k]=y->ptrs[k+root->mindeg];
     	}
      for(int k=0;k<root->mindeg-1;k++)
       z->keys[k]=y->keys[k+root->mindeg];
     
     y->n=root->mindeg-1;
     
    
     for(int k=root->n-1;k>=i;k--)
     root->keys[k+1]=root->keys[k];
     root->keys[i]=y->keys[root->mindeg-1];
     
      for(int j=root->n;j>=i+1;j--)
         root->ptrs[j+1]=root->ptrs[j];
     root->ptrs[i+1]=z;
     root->n=root->n+1;
  }
  void traverse(struct BtreeNode *root)
  {
    int i;
    for(i=0;i<root->n;i++)
    {
        if(root->leaf==0)
        traverse(root->ptrs[i]);
      printf(" %d",root->keys[i].roll_no);
    }
   if(root->leaf==0)
    traverse(root->ptrs[i]);
  }
 struct BtreeNode* search(int data1,struct BtreeNode *root)
  {
    int j=root->n-1;
    while(j>=0&&data1<root->keys[j].roll_no)
    j--;
    if(j>=0&&root->keys[j].roll_no==data1)
     return root;
    if(root->leaf==1)
      return NULL;
    return search(data1,root->ptrs[j+1]);
   }
 
  int search1(int k,struct BtreeNode **root)
  {
    //return (*root == NULL)? 0:search(k,*root);
    if ((search(k,*root)== NULL) || (*root == NULL))
		return NULL;
	else
	{
		struct BtreeNode* rec_found = search(k,*root);
		for(int i=0;i<rec_found->n;i++)
		{
			if (k==rec_found->keys[i].roll_no)
				return rec_found->keys[i].record;
		}
	
	}
  }
  void traverse1(struct BtreeNode **root)
  {
    if((*root)!=NULL)
    traverse(*root);
  }
  void insertion(int keys,struct BtreeNode **root,int rec)
  {
  if(*root==NULL)
    {
      *root=(struct BtreeNode*)malloc(sizeof(struct BtreeNode));
     (*root)->leaf=1;
     (*root)->mindeg=mind;
     (*root)->keys=(struct BtreeKey*)malloc(sizeof(struct BtreeKey)*(2*mind-1));
     (*root)->n=0;
     (*root)->ptrs=(struct BtreeNode**)malloc(sizeof(struct BtreeNode*)*(2*mind));
        (*root)->keys[0].roll_no=keys;
        (*root)->keys[0].record=rec;
        (*root)->n=1;
    }
    else
    {
        if((*root)->n==2*mind-1)
        {
            struct BtreeNode *newr=(struct BtreeNode*)malloc(sizeof(struct BtreeNode));
     newr->leaf=0;
     newr->mindeg=mind;
     newr->keys=(struct BtreeKey*)malloc(sizeof(struct BtreeKey)*(2*mind-1));
     newr->n=0;
     newr->ptrs=(struct BtreeNode**)malloc(sizeof(struct BtreeNode*)*(2*mind));
            newr->ptrs[0]=*root;
            splitnode(*root,0,newr);
            int j=0;
            if(newr->keys[0].roll_no<keys)
            j++;
            ins(keys,newr->ptrs[j],rec);
            *root=newr;
        }
     else
     ins(keys,*root,rec);
    }
  }
  void deletion1(int keys,struct BtreeNode **root)
  {
     if(!(*root))
     {
       printf("The tree is empty\n");
       return;
     }
     struct BtreeNode *curr=*root;
     deletion(keys,*root);
     if((*root)->n==0)
     {
        if((*root)->leaf)
           (*root)=NULL;
        else
           (*root)=(*root)->ptrs[0];
     
         free(curr);
     }
   }

int max(int a,int b)
{
if(a>b)
return a;
return b;
}
int min(int a,int b)
{
if(a<b)
return a;
return b;
}           
int main() {
  init_db();
  mind=3;
  
          int ch1;
	printf("	     <---------   DataBase System    ------->    \n\n");
	printf("1 ) Load Database from an existing file\n2 ) Create a new Database\n\nEnter Option : ");
	scanf("%d",&ch1);
        int j;
       struct BtreeNode* root=NULL;
	if (ch1 == 1)
	{
		
                 char rder[1000];
                 FILE *fptr;
                 if ((fptr = fopen("data.txt", "r")) == NULL) {
                        printf("Error! opening file");
                    // Program exits if file pointer returns NULL.
                     exit(1);
                        }
        	printf("The name of the table is:");
  		char tablename2[30]="studenttable";
                char inputarr[200];
                printf("%s\n",tablename2);
  		fgets(inputarr, 512, fptr);
                
  		printf("The number of columns for this table are:");
  		int numcols;
  //while ((getchar()) != '\n');
  		//scanf("%d",&numcols);
  		int i,j;
    		char *tablename=(char *)malloc(30 * sizeof(char));
                strcpy(tablename,tablename2);
  		
  		//fgets(inputarr,512,stdin);
   
  		char newString[200][200];
  		int ctr=0;
  		j=0;
  //int i;
  		for(i=0;i<strlen(inputarr);i++)
  		{
        // if space or NULL found, assign NULL into newString[ctr]
  		if(inputarr[i]==' '||inputarr[i]=='\0' ||inputarr[i]=='\n')
    		{
      			newString[ctr][j]='\0';
      			ctr++;  //for next word
      			j=0;    //for next word, init index to 0
     		}
    		else
    		{
    		 newString[ctr][j]=inputarr[i];
     			j++;
     		}
   		}
                numcols=ctr;
                printf("%d\n",numcols);
                char *colname;
  		char **columns;
                create_table(tablename,numcols);
  		printf("Table is created\n");
  //return 0;
  //fflush(stdin);
  		//while ((getchar()) != '\n');
  		Table *tablename1 = find_table(tablename);
  		
  		
  		columns=(char**)malloc((tablename1->numcols+1)*sizeof(char*));
  		for(i=0;i<tablename1->numcols;i++)
  		{
   
   //fgets(colname,512,stdin);
   		colname=(char *)malloc(30 * sizeof(char));
   //scanf("%s",colname);
   //while ((getchar()) != '\n');
  // printf("%s\n",colname);
   		strcpy(colname,newString[i]);
   		columns[i]=colname;
   		add_column(tablename1, colname);
                if(i==0)
                tablename1->keycol=colname;
  		}
  		int flag=1;
                   char **select_terms=columns;
                while(fgets(inputarr, 512, fptr)!=NULL)
  		{
      
                 //printf("\nEnter the Insert command values along with table name\n");


                   char **thosecolvalues1=(char**)malloc((tablename1->numcols+1)*sizeof(char*));
      
       
                  ///fgets(inputarr,512,stdin);
   
                  char newString[200][200];
                  int ctr=0;
                   j=0;
  //int i;
                  for(i=0;i<strlen(inputarr);i++)
                  {
                  if(inputarr[i]==' '||inputarr[i]=='\0' ||inputarr[i]=='\n')
                  {
                   newString[ctr][j]='\0';
                   ctr++;  //for next word
                    j=0;    //for next word, init index to 0
                  }
                 else
                 {
                 newString[ctr][j]=inputarr[i];
                 j++;
                 }
                }
               for(i=0;i<tablename1->numcols;i++)
                {
        
               thosecolvalues1[i]=(char *)malloc(30 * sizeof(char));
               strcpy(thosecolvalues1[i],newString[i]);
       //fflush(stdin);
               }
       
               insert_into(tablename1,columns,thosecolvalues1,tablename1->numcols);

              int roll_number=0;
     
              i=0;

             while(newString[0][i]!='\n'&&newString[0][i]!='\0')
             {
              roll_number=roll_number*10;

             roll_number+=(newString[0][i]-'0');
             i++;

             }
          //hash_t *n=select_from(tablename1, select_terms, numcols,tablename1->numofrows);
          insertion(roll_number,&root,tablename1->numofrows);
                tablename1->numofrows++;
       // printf("Type 0 if u want to stop taking inputs else type 1:");

      // scanf("%d",&flag);
                         //while ((getchar()) != '\n');
    }


}
  else
  {
  printf("Enter the name of the table:");
  char tablename2[30];
  fgets(tablename2,512,stdin);
  printf("Enter the number of columns that u want to create:");
  int numcols;
  //while ((getchar()) != '\n');
  scanf("%d",&numcols);
  int i,j;
    char *tablename=&tablename2[0];
  create_table(tablename,numcols);
  printf("Table is created\n");
  //return 0;
  //fflush(stdin);
  while ((getchar()) != '\n');
  Table *tablename1 = find_table(tablename);
  char *colname;
  char **columns;
  columns=(char**)malloc((tablename1->numcols+1)*sizeof(char*));
  char inputarr[200];
  fgets(inputarr,512,stdin);
   
  char newString[200][200];
  int ctr=0;
  j=0;
  //int i;
  for(i=0;i<strlen(inputarr);i++)
  {
  if(inputarr[i]==' '||inputarr[i]=='\0' ||inputarr[i]=='\n')
    {
      newString[ctr][j]='\0';
      ctr++;  //for next word
      j=0;    //for next word, init index to 0
     }
    else
    {
     newString[ctr][j]=inputarr[i];
     j++;
     }
   }
  for(i=0;i<tablename1->numcols;i++)
  {
   
   //fgets(colname,512,stdin);
   colname=(char *)malloc(30 * sizeof(char));
   
   strcpy(colname,newString[i]);
   columns[i]=colname;
   add_column(tablename1, colname);
   if(i==0)
   {
    tablename1->keycol=colname;
   }
  }
  int flag=1;
  list_columns(tablename1);
    //char *columns[] = {"first_name", "last_name", "address","Phonenumber"};
   char **select_terms=columns;
  while(flag)
  {
      
       printf("\nYou are doing the insertion of values in the table now\n");


                   char **thosecolvalues1=(char**)malloc((tablename1->numcols+1)*sizeof(char*));
      

       printf("Enter the values:");
       char inputarr[200];

  fgets(inputarr,512,stdin);
   
  char newString[200][200];
  int ctr=0;
  j=0;
  //int i;
  for(i=0;i<strlen(inputarr);i++)
  {
  if(inputarr[i]==' '||inputarr[i]=='\0' ||inputarr[i]=='\n')
    {
      newString[ctr][j]='\0';
      ctr++;  //for next word
      j=0;    //for next word, init index to 0
     }
    else
    {
     newString[ctr][j]=inputarr[i];
     j++;
     }
   }
       for(i=0;i<tablename1->numcols;i++)
       {
        
        thosecolvalues1[i]=(char *)malloc(30 * sizeof(char));
        strcpy(thosecolvalues1[i],newString[i]);
       //fflush(stdin);
       }
       
        insert_into(tablename1,columns,thosecolvalues1,tablename1->numcols);

       /*for(i=0;i<tablename1->numcols;i++)
       {
       printf("%s\n",thosecolvalues1[i]);
       }*/
        int roll_number=0;
     
    i=0;

    while(newString[0][i]!='\n'&&newString[0][i]!='\0')
    {
         roll_number=roll_number*10;

         roll_number+=(newString[0][i]-'0');
         i++;

    }
          //hash_t *n=select_from(tablename1, select_terms, numcols,tablename1->numofrows);
          insertion(roll_number,&root,tablename1->numofrows);
                tablename1->numofrows++;
        printf("Type 0 if u want to stop taking inputs else type 1:");

       scanf("%d",&flag);
                         while ((getchar()) != '\n');
    }
  
  char *values[] = {"2","John", "Doe", "3 Kensington Lane"};
  insert_into(tablename1, columns, values, tablename1->numcols);
           //hash_t *n=select_from(tablename1, select_terms, numcols,tablename1->numofrows);
            insertion(2,&root,tablename1->numofrows);
            tablename1->numofrows++;
  char *values2[] = {"3","Bruce", "Wayne", "20 Hyde Park Corner"};
  insert_into(tablename1, columns, values2, tablename1->numcols);
 // n=select_from(tablename1, select_terms, numcols,tablename1->numofrows);
            insertion(3,&root,tablename1->numofrows);
            tablename1->numofrows++;

  //char *select_terms[] = {"first_name", "last_name", "address"};
  int num_columns = numcols;
  }
    list_tables();
    printf("Select one table of the given choices\n");
    char tbname[20];
    scanf("%s",tbname);
      Table *tablename1 = find_table(tbname);
         int numcols=tablename1->numcols;
         char **select_terms=(char**)malloc((tablename1->numcols+1)*sizeof(char*));
         int i=0;
      hash_each_key(tablename1->columns, {
    printf("%s\n", key);
    select_terms[i]=(char*)malloc(30*sizeof(char));
    strcpy(select_terms[i],key);
    i++;
  })
  while(1)
  {
  int choice;
  Table *tablename1 = find_table(tbname);
         int numcols=tablename1->numcols;
         char **select_terms=(char**)malloc((tablename1->numcols+1)*sizeof(char*));
         int i=0;
      hash_each_key(tablename1->columns, {
    //printf("%s\n", key);
    select_terms[i]=(char*)malloc(30*sizeof(char));
    strcpy(select_terms[i],key);
    i++;
  })
  printf("Choose an option\n1.Select all rows in the table\n2.Select through the column_name\n3.Select through key\n4.Delete a row\n5.Update a row\n6.Drop a column\n7.Add a column\n8.Maximum value of a given column\n9.Minimum value of a given column\n10.Sum of all values of a given column\n11.Average of all values of a given column\n12.Save the table\n13.Exit:\nSelect your choice:");
  scanf("%d",&choice);
  if(choice==1)
  {
  for(i=0;i<tablename1->numofrows;i++)
  {
   hash_t *result=select_from(tablename1, select_terms, numcols, i);
   if (result) {
    hash_each(result, {
      printf("%s: %s\n", key, val);
    })
    hash_free(result);
  }
  }
  }
  else if(choice==2)
  {
  printf("Give the number of columns u want to query on:\n");
  int cols;
  scanf("%d",&cols);

  printf("\nGive the column names and its values on what you are making a search query:");
  char colnam[tablename1->numcols][30],colvalu[tablename1->numcols][30],anscol[30],ansval[30];
  //char **colname=colnam,**colvalue=colvalu;
  char **colname=(char**)malloc(tablename1->numcols*sizeof(char*));
  char **colvalue=(char**)malloc(tablename1->numcols*sizeof(char*));
  for(int j=0;j<cols;j++)
  {
  scanf("%s",colnam[j]);
  colname[j]=(char*)malloc(30*sizeof(char));
  strcpy(colname[j],colnam[j]);
  scanf("%s",colvalu[j]);
    colvalue[j]=(char*)malloc(30*sizeof(char));
  strcpy(colvalue[j],colvalu[j]);
  }
  int arr[tablename1->numofrows],cnt=0;
  cnt=where_equals(tbname, colname, colvalue,cols,arr,cnt);
   for(i=0;i<cnt;i++)
  {
   hash_t *result=select_from(tablename1, select_terms, numcols, arr[i]);
   if (result) {
    hash_each(result, {
      printf("%s: %s\n", key, val);
    })
    hash_free(result);
  }
  }
  free(colname);
  free(colvalue);
  }
  else if(choice==3)
  {
    int key;
    printf("Enter the primary-key value:");
    scanf("%d",&key);
    (search1(key,&root) != NULL)? printf("\nPresent\n") :printf("\nNot Present");
    int res= search1(key,&root);
       hash_t *result=select_from(tablename1, select_terms, tablename1->numcols, res);
    if (result) {
    hash_each(result, {
      printf("%s: %s\n", key, val);
    })
    hash_free(result);
  }
  }
  else if(choice==4)
  {
  printf("Give the number of columns u want to query on:\n");
  int cols;
  scanf("%d",&cols);

  printf("\nGive the column names and its values on what you want to make the deletion of row/rows:");
char colnam[tablename1->numcols][30],colvalu[tablename1->numcols][30],anscol[30],ansval[30];
  char **colname=(char**)malloc(tablename1->numcols*sizeof(char*));
  char **colvalue=(char**)malloc(tablename1->numcols*sizeof(char*));
  for(int j=0;j<cols;j++)
  {
  scanf("%s",colnam[j]);
  colname[j]=(char*)malloc(30*sizeof(char));
  strcpy(colname[j],colnam[j]);
  scanf("%s",colvalu[j]);
    colvalue[j]=(char*)malloc(30*sizeof(char));
  strcpy(colvalue[j],colvalu[j]);
  }
   int arr[tablename1->numofrows],cnt=0;
  cnt=where_equals(tbname, colname, colvalue,cols,arr,cnt);
   //int rowindex=where_equals(tbname, colname, colvalue);
   for(i=0;i<cnt;i++)
  {
      hash_t *result=select_from(tablename1, select_terms, tablename1->numcols, arr[i]);
   if (result) {
    hash_each(result, {
     char newkey[30];
    strcpy(newkey,key);
     if(strcmp(newkey,tablename1->keycol)==0)
     {
        int roll_number=0;
     
          j=0;
       // printf("Key column is:%s\n",newkey);
    char newval[30];
    strcpy(newval,val);
    while(newval[j]!='\n'&&newval[j]!='\0')
    {
         roll_number=roll_number*10;

         roll_number+=(newval[j]-'0');
         j++;

    }
    //printf("Going to delete the entry in btree as well the value:%d\n",roll_number);
     deletion1(roll_number,&root);
     }
    })
    hash_free(result);
   }
     Row *row = tablename1->rows->data[arr[i]];
     drop_row(tablename1,row);
     
    tablename1->numofrows--;
     printf("A row is deleted\n");
  }
  free(colname);
  free(colvalue);
  }
  else if(choice==5)
  {

     printf("Give the number of columns u want to query on:\n");
  int cols;
  scanf("%d",&cols);

     printf("\nGive the column name and its value on what you want to make the updation of the row");
  char colnam[tablename1->numcols][30],colvalu[tablename1->numcols][30],anscol[30],ansval[30];
 // char **colname=colnam,**colvalue=colvalu;
  char **colname=(char**)malloc(tablename1->numcols*sizeof(char*));
  char **colvalue=(char**)malloc(tablename1->numcols*sizeof(char*));
  for(int j=0;j<cols;j++)
  {
  scanf("%s",colnam[j]);
  colname[j]=(char*)malloc(30*sizeof(char));
  strcpy(colname[j],colnam[j]);
  scanf("%s",colvalu[j]);
    colvalue[j]=(char*)malloc(30*sizeof(char));
  strcpy(colvalue[j],colvalu[j]);
  }
     int arr[tablename1->numofrows],cnt=0;
  cnt=where_equals(tbname, colname, colvalue,cols,arr,cnt);

   //colvalue=(char**)malloc(tablename1->numcols*sizeof(char*));
   for(int j=0;j<cols;j++)
   {
  printf("Type here the new-value: of those column that you want to update:");
  char newval[30];
  scanf("%s",newval);
  //colvalue[j]=(char*)malloc(30*sizeof(char));
  strcpy(colvalue[j],newval);
   }
   for(i=0;i<cnt;i++)
  {
     Row *row = tablename1->rows->data[arr[i]];
     
    hash_t *result=select_from(tablename1, select_terms, tablename1->numcols, arr[i]);
             int roll_number=0;
   if (result) {
    hash_each(result, {
     if(strcmp(key,tablename1->keycol)==0)
     {

     
          j=0;
    char newval[30];
    strcpy(newval,val);
    while(newval[j]!='\n'&&newval[j]!='\0')
    {
         roll_number=roll_number*10;

         roll_number+=(newval[j]-'0');
         j++;

    }
     deletion1(roll_number,&root);
     }
    })
    hash_free(result);
   }
     for(int j=0;j<cols;j++)
   {
     hash_set(row->cells, colname[j], colvalue[j]);

    }
              result=select_from(tablename1, select_terms, tablename1->numcols, arr[i]);
     if (result) {
    hash_each(result, {
     if(strcmp(key,tablename1->keycol)==0)
     {

     
          j=0;
    char newval[30];
    strcpy(newval,val);
    roll_number=0;
    while(newval[j]!='\n'&&newval[j]!='\0')
    {
         roll_number=roll_number*10;

         roll_number+=(newval[j]-'0');
         j++;

    }
     //deletion1(roll_number,&root);
     }
    })
   // hash_free(result);
   }
                   insertion(roll_number,&root,arr[i]);
     printf("The row is updated\n");
         //hash_t *result=select_from(tablename1, select_terms, numcols, arr[i]);
   if (result) {
    hash_each(result, {
      printf("%s: %s\n", key, val);
    })
   hash_free(result);
  }
  }
  free(colname);
  free(colvalue);   
  }
  else if(choice==6)
  {
    char colname[30];
    printf("Enter the name of the column that u want to drop:");
    scanf("%s",colname);
    Column *column=(Column*)malloc(sizeof(Column));
    column->name=(char*)malloc(30*sizeof(char));
    strcpy(column->name,colname);
    drop_column(tablename1,column);
    tablename1->numcols--;
  }
  else if(choice==7)
  {
    
    char colname[30];
    printf("Enter the name of the column that u want to add:");
    scanf("%s",colname);
    char *column;
    column=(char*)malloc(30*sizeof(char));
    strcpy(column,colname);
    add_column(tablename1,column);
        tablename1->numcols++;
  }
  else if(choice==8)
  {
     char colname[30];
    printf("Enter the name of the column that u want the Maximum value of all the tuples:");
    scanf("%s",colname);
    int maxi=0;
    for(i=0;i<tablename1->numofrows;i++)
  {
      hash_t *result=select_from(tablename1, select_terms, tablename1->numcols,i);
              int roll_number=0;
   if (result) {
    hash_each(result, {
     char newkey[30];
    strcpy(newkey,key);
     if(strcmp(newkey,colname)==0)
     {

     
          j=0;
        //printf("Key column is:%s\n",newkey);
    char newval[30];
    strcpy(newval,val);
    while(newval[j]!='\n'&&newval[j]!='\0')
    {
         roll_number=roll_number*10;

         roll_number+=(newval[j]-'0');
         j++;

    }

    
     }
    })
    hash_free(result);
    maxi=max(roll_number,maxi);
   }
  }
         printf("Maximum value in %s is:%d\n",colname,maxi);
  }
  else if(choice==9)
  {
  char colname[30];
    printf("Enter the name of the column that u want the Minimum value of all the tuples:");
    scanf("%s",colname);
    int maxi=INT_MAX;
    for(i=0;i<tablename1->numofrows;i++)
  {
      hash_t *result=select_from(tablename1, select_terms, tablename1->numcols,i);
              int roll_number=0;
   if (result) {
    hash_each(result, {
     char newkey[30];
    strcpy(newkey,key);
     if(strcmp(newkey,colname)==0)
     {

     
          j=0;
        //printf("Key column is:%s\n",newkey);
    char newval[30];
    strcpy(newval,val);
    while(newval[j]!='\n'&&newval[j]!='\0')
    {
         roll_number=roll_number*10;

         roll_number+=(newval[j]-'0');
         j++;

    }

    
     }
    })
    hash_free(result);
    maxi=min(roll_number,maxi);
   }
  }
       printf("Minimum value in %s is:%d\n",colname,maxi);
  }
  else if(choice==10)
  {
   char colname[30];
    printf("Enter the name of the column that u want the sum of all the tuples:");
    scanf("%s",colname);
    int sum=0;
    for(i=0;i<tablename1->numofrows;i++)
  {
      hash_t *result=select_from(tablename1, select_terms, tablename1->numcols,i);
              int roll_number=0;
   if (result) {
    hash_each(result, {
     char newkey[30];
    strcpy(newkey,key);
     if(strcmp(newkey,colname)==0)
     {

     
          j=0;
        //printf("Key column is:%s\n",newkey);
    char newval[30];
    strcpy(newval,val);
    while(newval[j]!='\n'&&newval[j]!='\0')
    {
         roll_number=roll_number*10;

         roll_number+=(newval[j]-'0');
         j++;

    }

    
     }
    })
    hash_free(result);
    sum+=roll_number;
   }
  }
     printf("Sum of %s is:%d\n",colname,sum);
  }
  else if(choice == 11)
  {
  char colname[30];
    printf("Enter the name of the column that u want the sum of all the tuples:");
    scanf("%s",colname);
    int sum=0;
    for(i=0;i<tablename1->numofrows;i++)
  {
      hash_t *result=select_from(tablename1, select_terms, tablename1->numcols, i);
              int roll_number=0;
   if (result) {
    hash_each(result, {
     char newkey[30];
    strcpy(newkey,key);
     if(strcmp(newkey,colname)==0)
     {

     
          j=0;
        //printf("Key column is:%s\n",newkey);
    char newval[30];
    strcpy(newval,val);
    while(newval[j]!='\n'&&newval[j]!='\0')
    {
         roll_number=roll_number*10;

         roll_number+=(newval[j]-'0');
         j++;

    }
     }
    })
    hash_free(result);
    sum+=roll_number;
   }
   }
   printf("Average of %s is:%f\n",colname,(sum*1.0)/tablename1->numofrows);
  }
  else if (choice==12)
		{
			FILE *fptr;
    			if ((fptr = fopen("student_data.txt", "w")) == NULL) 
    			 {
      			printf("Error! opening file");
                    // Program exits if file pointer returns NULL.
                       exit(1);
                         }
                        fputs(tablename1->keycol,fptr);
                        fputs(" ",fptr);
                       for(int i=0;i<tablename1->numcols;i++)
                       {
                                if(strcmp(select_terms[i],tablename1->keycol)!=0)
                                {fputs(select_terms[i],fptr);
                                  if(i!=tablename1->numcols-1)
                                fputs(" ",fptr);}                        
                       }
                                                     fputs("\n",fptr);
                       for(i=0;i<tablename1->numofrows;i++)
                        {
                         
                        hash_t *result=select_from(tablename1, select_terms, tablename1->numcols, i);
                              if (result) {
                                hash_each(result, {
                                       char newkey[30];
                                       strcpy(newkey,key);
                                       if(strcmp(newkey,tablename1->keycol)==0)
                                       {
                                        int roll_number=0;
                                        
                                         j=0;
                                         char newval[30];
                                         strcpy(newval,val);
                                         fputs(newval,fptr);
                                         fputs(" ",fptr);
                                           }
                                           })
                                        }
                                  int j=0;
                                  if (result) {
                                   
                                   hash_each(result, {
                                  char newkey[30];
                                       strcpy(newkey,key);
                                       if(strcmp(newkey,tablename1->keycol)!=0)
                                       {
                                       
                                         char newval[30];
                                         strcpy(newval,val);
                                         fputs(newval,fptr);
                                         if(j<tablename1->numcols-2)
                                         fputs(" ",fptr);
                                         else
                                         fputs("\n",fptr);
                                         j++;
                                       }
                                           })
                              hash_free(result);
                                   }

                          }
			
                   fclose(fptr);
		}
  else
  break;
  }
  free_db();
  return 0;
}
